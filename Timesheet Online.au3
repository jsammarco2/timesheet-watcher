#include <GUIConstantsEx.au3>
#include <StaticConstants.au3>
#include <WindowsConstants.au3>
#include <Constants.au3>
#include <InetConstants.au3>

$title = "Timesheet Watcher"
#Region ### START Koda GUI section ### Form=c:\users\sesa466246\desktop\timesheet online\form.kxf
$Form = GUICreate($title, 584, 104, 192, 124, BitOR($GUI_SS_DEFAULT_GUI,$WS_MAXIMIZEBOX,$WS_SIZEBOX,$WS_THICKFRAME,$WS_TABSTOP))
$label = GUICtrlCreateLabel("LOADING. . .", 0, 0, 581, 97, $SS_CENTER)
GUICtrlSetFont(-1, 48, 800, 0, "Rockwell")
GUICtrlSetResizing(-1, $GUI_DOCKAUTO)
GUISetState(@SW_SHOW)
#EndRegion ### END Koda GUI section ###
Global $DOS, $Message = '', $lastState = -1, $timestamp = TimerInit()
checkTimesheetOnline()
Sleep(1000)
GUISetState(@SW_MINIMIZE)
AdlibRegister("checkTimesheetOnline", 10000)

While 1
	$nMsg = GUIGetMsg()
	Switch $nMsg
		Case $GUI_EVENT_CLOSE
			Exit

	EndSwitch
WEnd


Func checkTimesheetOnline()
	$DOS = Run(@ComSpec & " /c tcping.exe -n 1 dynslweb.schneider-electric.com", "", @SW_HIDE, $STDERR_CHILD + $STDOUT_CHILD)
	While 1
		Sleep(10) ;; no need to go at full speed here.
		$Message &= StdoutRead($DOS) ;; ditched peek parameter + changed "=" to "&=" ... No more endless loop.
		if @error then exitloop
	WEnd
	ConsoleWrite($Message&@CRLF)
	If StringInStr($Message, "Could not find host") = 0 Then
		;ONLINE
		GUICtrlSetColor($label, "0x00FF00")
		GUICtrlSetData($label, "Timesheet Online")
		If $lastState <> 1 Then
			$timeSinceLastState = Round(TimerDiff($timestamp)/1000/60,0)
			TrayTip("Timesheet is now Online", "It was offline for " & $timeSinceLastState & " minute(s)", 4, $TIP_ICONASTERISK)
		EndIf
		$timestamp = TimerInit()
		$lastState = 1
	Else
		;OFFLINE
		GUICtrlSetColor($label, "0xFF0000")
		GUICtrlSetData($label, "Timesheet Offline")
		If $lastState <> 0 Then
			$timeSinceLastState = Round(TimerDiff($timestamp)/1000/60,0)
			TrayTip("Timesheet is now Offline", "It was online for " & $timeSinceLastState & " minute(s)", 4, $TIP_ICONEXCLAMATION)
		EndIf
		$timestamp = TimerInit()
		$lastState = 0
	EndIf
	;MsgBox(0, "Stdout Read:", $Message)
	;ConsoleWrite(FileRead(@TempDir&"\TimesheetOnline.tmp")&@CRLF&$iFileSize&@CRLF)
EndFunc